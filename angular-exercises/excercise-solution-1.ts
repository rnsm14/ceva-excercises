// Problems noticed:

// 	• 'concatMap' and 'timer' within the 'concat' function is correct but it can deal with issues in the future. It should be used instead 'switchMap' to switch to a new observable everytime the query changes and cancel the previous request. Once 'switchMap' is used, it is no longer needed to create a timer to emit values periodically, instead it should be used the 'interval' function for that purpose.
// 	• It is adviced to use an Input Debouncing where it is going to add a debounce time to the query observable to avoid making too many requests when the user is typying quickly.
// Error Handling: it is important to handle errors when when there is an observable chain.

// Code improved:

// 	1. 'switchMap' is used to the latest search result when the query changes.
// 	2. User time input is debounced by 300 milliseconds in order to reduce the frequency of requests to the 'findUsers' service.
// 	3. Errors are handled using the 'catchError' operator and provide a default empty array if there is an error.
// 	4. 'onQueryChange' method is created in order to update the 'querySubject' when the value changes.
// 	5. Better Angular way of handling input changes: 'ngModelChange' is updated it to 'querySubject', that is why I would use the '(input)' event and two-way binding with '[(ngModel)]'.
// 	6. In order to avoid memory leaks I would also unsubscribe with 'ngOnDestroy' function from the observable when the component is destroyed.
	
// Solution in code:

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, switchMap, catchError } from 'rxjs/operators';
import { UserService } from './user.service';

@Component({
  selector: 'app-users',
  template: `
    <input type="text" [(ngModel)]="query" (input)="onQueryChange()">
    <div *ngFor="let user of users">
        {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit, OnDestroy {
  query = '';
  querySubject = new Subject<string>();
  users: { email: string }[] = [];
  private querySubscription: Subscription;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.querySubscription = this.querySubject
      .pipe(
        debounceTime(300),
        switchMap(q => this.userService.findUsers(q).pipe(catchError(error => [])))
      )
      .subscribe({
        next: (res) => (this.users = res),
        error: (error) => {
          console.error('Error:', error);
        }
      });
  }

  ngOnDestroy(): void {
    if (this.querySubscription) {
      this.querySubscription.unsubscribe();
    }
  }

  onQueryChange(): void {
    this.querySubject.next(this.query);
  }
}
