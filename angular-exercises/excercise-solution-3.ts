// Solution proposed:

// 	1. Import the required Angular modules and clases.
// 	2. Create a Reactive Form for the user data structure.
// 	3. Add validation for each form control on the provided criteria (ex. 'Validators.required', regExpressions and others).
// 	4. Modify the HTML template to bind the form controls.
// 	5. Add a button to submit the form data.
// 	6. Modify the 'doSubmit' method to emit the form data.

// Code solutions and improvements:

// 1. I would create a 'FormGroup' called 'userForm' and add form controls for each field with the necessary validators.
// 2. I would add a button in the HTML template to submit the form and I would edit the 'doSubmit' method in order to check if the form is valid and emits the form data if it is. Also I would validate the birthday field by the function 'validateBirthday()'.

import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  template: `
    <form [formGroup]="userForm" (ngSubmit)="doSubmit()">
      <input type="text" formControlName="email" placeholder="Email">
      <input type="text" formControlName="name" placeholder="Name">
      <input type="date" formControlName="birthday" placeholder="Birthday">
      <input type="number" formControlName="zip" placeholder="Zip">
      <input type="text" formControlName="city" placeholder="City">
      <button type="submit" [disabled]="userForm.invalid">Submit</button>
    </form>
  `
})
export class AppUserForm {
  @Output() event = new EventEmitter<{
    email: string;
    name: string;
    birthday: Date;
    address: { zip: number; city: string };
  }>();

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.maxLength(128)]],
      birthday: [null, this.validateBirthday],
      zip: ['', [Validators.required]],
      city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z\s]+$/)]],
    });
  }

  validateBirthday(control) {
    if (control.value) {
      const birthday = new Date(control.value);
      if (birthday >= new Date()) {
        return { futureDate: true };
      }
    }
    return null;
  }

  doSubmit(): void {
    if (this.userForm.valid) {
      const formData = {
        email: this.userForm.value.email,
        name: this.userForm.value.name,
        birthday: this.userForm.value.birthday,
        address: {
          zip: this.userForm.value.zip,
          city: this.userForm.value.city,
        },
      };
      this.event.emit(formData);
    }
  }
}
