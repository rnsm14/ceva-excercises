// Improvements:
	
// • Optimize the 'ngFor' loop in the template and function 'getCapitalizeFirstWord'. 

// How to start improving:

// 1. Change Detection Strategy: set the change detection strategy to 'OnPush' for reducing the frequency of change detection, checks and enhance perfomance.
// 2. Pure Pipes: I would create a custom pure pipe to handle the string transformation which will improve performance by catching the reults and only recalculating when its necessary.
// TrackBy Function: I would set a 'trackBy' function for the 'ngFor' loop to help Angular to identify unique items and update the DOM more efficiently.

// Code Improved:

// 	• I would create a custom 'CapitalizePipe' pipe that handles the capitalization of names. As it is a pure pipe it catches and recalculate the results only when the input changes.
// 	• Strategy 'OnPush' is set on the component in order to reduce the frequency of change detection checks.
// 	• 'trackBy' function is used to help Angular to identify unique users, making the rendering process more efficient.
// This improvements help specially when dealing with a large number of users.

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {
  transform(value: string): string {
    if (!value) return value;
    return value
      .split(' ')
      .map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase())
      .join(' ');
  }
}

@Component({
  selector: 'app-users',
  template: `
    <div *ngFor="let user of users; trackBy: trackByUser">
      {{ user.name | capitalize }}
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppUsers {
  @Input() users: { name: string }[];

  trackByUser(index: number, user: { name: string }): string {
    return user.name;
  }
}
