const searchText = "example@example.com";
const sixMonthsAgo = new Date();
sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);

// Solutions:

// Indexes should be creatd on the fields in the query to improve search perfomance. In this specific case, it should be created indexes on the 'email' and 'last_connection_date' fields.

//use users

db.users.createIndex({ email: 1 });
db.users.createIndex({ last_connection_date: -1 });

//The '{email: 1}' index is used for efficient searching by email and the  '{last_connection_date: -1}' index is used for sorting users by theis las connection date.

//It is advised to user the '$regex' operator for searching by email and the '$gte' operator for filtering users within the last 6 months. 'or' operator can be used for combining the conditions for email, first and last name.

const query = {
  $or: [
    // Case-insensitive email search
    { email: { $regex: searchText, $options: 'i' } },
    // Case-insensitive first name search 
    { first_name: { $regex: `^${searchText}`, $options: 'i' } },
    // Case-insensitive last name search 
    { last_name: { $regex: `^${searchText}`, $options: 'i' } }
  ],
  //Date sorting
  last_connection_date: { $gte: sixMonthsAgo },
};

const result = db.users.find(query);
