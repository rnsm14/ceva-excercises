//3.1 solution query

const objectIdToUpdate = ObjectId("5cd96d3ed5d3e20029627d4a");
const currentDate = new Date();

db.users.updateOne(
  { _id: objectIdToUpdate },
  { $set: { last_connection_date: currentDate } }
);

//3.2 solution query:

db.users.updateOne(
  { _id: objectIdToUpdate },
  { $addToSet: { roles: "admin" } }
);

//3.3 solution query:

db.users.updateOne(
  { _id: objectIdToUpdate, "addresses.zip": "75001" },
  { $set: { "addresses.$.city": "Paris 1" } }
);
