//Aggregation framework is used to group users by their roles and send and aggregation result where each document represents a role with an array of user emails for that role. 

// Stages used:

// 	1. '$group' stage groups users by their roles and creates an array of users emails for each role usinf the '$push' accumulator.
// 	2. As 'roles' is an array, it is used the '$unwind' stage to separate each role into its own document.
// 	3. '$project' stage is used to reshape the documents by only keeping the '_id' and 'users' fields.
// 	4. Another '$group' stage is used in to group documents by '_id' (role) and create an array of user arrays.
// Last '$project' stage renames the fields and omits the '_id' fields to achieve the desired structure of the results.


db.users.aggregate([
    {
      $group: {
        _id: '$roles',
        users: { $push: '$email' }
      }
    },
    {
    // As roles is an array, unwind it to have one role per document
      $unwind: '$_id' 
    },
    {
      $project: {
        _id: 1,
        users: 1
      }
    },
    {
      $group: {
        _id: '$_id',
        users: { $push: '$users' }
      }
    },
    {
      $project: {
        _id: 0,
        role: '$_id',
        users: 1
      }
    }
  ]);
  