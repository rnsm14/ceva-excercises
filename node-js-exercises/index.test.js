'use strict'

const got = require('got')

const { getCountUsers, getTotalVehicles, getPlural, url, urlVehicle  } = require('./index');

jest.mock('got');

describe('getCountUsers', () => {
  it('should return the total count of users', async () => {
    const mockResponse = {
      body: JSON.stringify({ total: 20 })
    };
    got.get.mockResolvedValue(mockResponse);

    const result = await getCountUsers(url);

    expect(got.get).toHaveBeenCalledWith(url);
    expect(result).toBe(20);
  });

  it('should handle errors', async () => {
    const mockError = new Error('Request failed');
    got.get.mockRejectedValue(mockError);

    const result = await getCountUsers(url);

    expect(got.get).toHaveBeenCalledWith(url);
    expect(result).toBe(0);
  });
});

describe('getTotalVehicles', () => {
  it('should return the total number of vehicles', async () => {
    const mockResponse = {
      body: '100',
    };
    got.get.mockResolvedValue(mockResponse);

    const urltotalVehicles = urlVehicle;
    const result = await getTotalVehicles(urltotalVehicles);

    expect(got.get).toHaveBeenCalledWith(urltotalVehicles);
    expect(result).toBe(100);
  });

  it('should handle function errors', async () => {
    const mockError = new Error('Request failed');
    got.get.mockRejectedValue(mockError);

    const urltotalVehicles = urlVehicle;
    const result = await getTotalVehicles(urltotalVehicles);

    expect(got.get).toHaveBeenCalledWith(urltotalVehicles);
    expect(result).toBe(0);
  });
});

describe('getPlural', () => {
  it('should return "none" when total value is <= 0', async () => {
    getTotalVehicles.mockResolvedValue(0);

    const result = await getPlural();
    expect(result).toBe('none');
  });

  it('should return "few" when total value is <= 10', async () => {
    getTotalVehicles.mockResolvedValue(10);

    const result = await getPlural();
    expect(result).toBe('few');
  });

  it('should return "many" when total value is > 10', async () => {
    getTotalVehicles.mockResolvedValue(20);

    const result = await getPlural();
    expect(result).toBe('many');
  });

  it('should return "error" when getPlural has invalid data', async () => {
    getTotalVehicles.mockRejectedValue(new Error('Request failed'));

    const result = await getPlural();
    expect(result).toBe('error');
  });
});