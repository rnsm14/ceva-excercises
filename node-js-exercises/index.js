'use strict'

// Solution:
// 	• The 'got' library must be imported in order to use its functions, properties and methods.
// 	• The way function 'getCountUsers()' called is going to fail due:
// 	1. 'getCountUsers()' is an asynchronous function so it must be used an 'await' keyword in order to get the result and to access the 'total' property.
// 	2. In order to improve the asynchronous functions it must be added the try and catch blocks for better response and errors management.
// 	3. Also it is important to return the results as wanted to be handled. Ex. JSON format, array, string, local response.
// In order to not repeat the url it should be declared in variables in order to use it dynamically in other functions and make the code more readable.

//Code Solution:

const got = require('got')

//1st exercice

const url = "https://my-webservice.moveecar.com/users/count";
const urltotalVehicles = "https://my-webservice.moveecar.com/users/count";

async function getCountUsers() {
  try {
        const response = await got.get(url);
        const data = JSON.parse(response.body);
    return data.total;
  } catch (error) {
    console.log(error);
    return 0;
  }
}

getCountUsers();

async function computeResult() {
    const result = await getCountUsers();
    return result + 20;
  };
  
computeResult().then(result => console.log(result));

//2nd excercice

// Problem identified:
// Is also related to the asynchronous nature of 'getTotalVehicles()' and how is tried to access the 'total' variable before it has been assigned to a value.

// Solution:

// 	1. Await was added when calling 'getTotalVehicles()' to ensure that 'total' variable is assigned the result of the asynchronous operation before it is used.
// 	2. Proper handling error for the HTTP request and parsing.
// 	3. Library got must be imported.
// Also conditions were transformed into a ternary in order to save lines of code, making it faster and easier to read.

//Bonus: see tests.

async function getTotalVehicles() {
    try {
        const response = await got.get(urltotalVehicles);
        return parseInt(response.body);
    } catch (error) {
        console.log(error);
        return 0;
    }
};

async function getPlural() {
    try {
        const total = await getTotalVehicles();
        const result = total <= 0 ? 'none' : total <= 10 ? 'few' : 'many';
        return result;

        
    } catch (error) {
        console.log(error);
        return 'error';
    }
};

getPlural().then(result => console.log(result));

module.exports = { getCountUsers, computeResult, getTotalVehicles, getPlural }