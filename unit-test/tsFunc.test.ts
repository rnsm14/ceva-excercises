// Cases to test in function:

// 	1. Capitalize first word of a string
// 	2. Handle null input 
// 	3. Handle single-letter words

// In order to keep code clean and organized the function to be tested must be imported and more cases have to be tested, ex.if number is send by input and others.

const { getCapitalizeFirstWord } = require('./tsFunc')
const { expect, test } = require('@jest/globals')

test('Capitalize first word of a string', () => {
  expect(getCapitalizeFirstWord('ro san')).toBe('Ro San');
  expect(getCapitalizeFirstWord('ro')).toBe('Ro');
  expect(getCapitalizeFirstWord('')).toBe('');
});

test('Handles null input', () => {
  expect(() => {
    getCapitalizeFirstWord(null);
  }).toThrowError('Failed to capitalize first word with a null value');
});

test('Handles single-letter words', () => {
  expect(getCapitalizeFirstWord('x y z')).toBe('X Y Z');
});